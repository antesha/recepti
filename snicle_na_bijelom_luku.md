# Šnicle na bijelom luku

### Sastojci

- 400g svinjskih šnicli
- brašno
- 1 češanj bijelog luka
- slatka mljevena crvena paprika, vegeta, biber, sol

### Priprema

1. Šnicle začiniti vegetom, soli i biberom; umuljati ih u brašno i zapeći na ulju te nakon toga ulje baciti i očistiti posudu.
2. Vratiti šnicle i ubaciti bijeli luk, zaliti vodom taman da pokrije šnicle, posuti crvenom paprikom i ostaviti da krčka.
3. Pri kraju ako nije dovoljno "jako" dodati vegete i paprike