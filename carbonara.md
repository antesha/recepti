# Carbonara

## Sastojci
- 100g sira (edamer/gouda)
- 125g šunke u ovitku
- 200g vrhnje za kuhanje
- 500g tijesto
- 50g slanina

## Priprema

Kada je tijesto gotovo ubacimo sve narezane sastojke unutra, ne zaboraviti posoliti i nauljiti vodu u kojoj se tijesto kuha
