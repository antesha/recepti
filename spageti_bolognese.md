# Špageti bolognese

### Sastojci

- 2-3 glavice luka
- 1 češanj češnjaka
- 500g mljevenog mesa (svinjetina ili teletina)
- 1000g pasirane rajčice
- 500g špageta

### Priprema

1. Na zagrijanom ulje propržiti luk
1. Baciti češnjak i kratko popržiti mljeveno meso
1. Dodati pasiranu rajčicu začiniti i kuhati na laganoj vatri 15-20 minuta
1. Sa strane skuhati špagete i poslužiti


### Link

[Coolinarika](https://www.coolinarika.com/recept/jednostavni-bolognese-umak-sa-spagetima/)
