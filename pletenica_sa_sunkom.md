# Pletenica sa šunkom

## Sastojci

- tijesto
	- 500 g brašna za dizana tijesta
	- 200 g mlakog mlijeka
	- 1 svježi kvasac
	- 2 žličice šećera
	- 1 prašak za pecivo
	- 1 žlica soli
	- 70 ml tekućeg jogurta
	- 1 jaje
	- 30 g otopljenog maslaca
	
- nadjev
	- 200 g šunke u ovitku
	- 100 g masnijeg svježeg kravljeg sira
	- žlica milerama 150 g gaude ili nekog drugog topljivog sira po ukusu
	- pola crvene paprike
	- mladi luk
	- 1 jaje
	- 2 žlice senfa
	- žumanjak
	- malo mlijeka za premazivanje
	
## Priprema

1. Otopite kvasac u 200 ml mlakog mlijeka kojem ste dodali dvije žličice šećera. Brašno pomiješajte s praškom za pecivo i solju, u sredini napravite udubinu pa u nju ulijte kvasac rastopljen u mlijeku.
1. Ostavite da kvasac naraste pa dodajte sve preostale sastojke i zamijesite glatko tijesto. Ostavite na toplom da udvostruči obujam. Za to vrijeme pripremite nadjev. 3.Svježi kravlji sir pomiješajte s mileramom da dobijete kremu, posolite pa umiješajte naribani sir i razmućeno jaje.
1. Dodajate na sitne kockice narezanu papriku i mladi luk. Smjesa mora biti kremasta i ne previše rijetka
1. Nakon što je tijesto naraslo, razvaljajte ga u kvadrat veličine lima za pečenje. 6.Prenesite u lim pa srednju trećinu tijesta ostavite cijelu, a dvije postranične, na jednakom razmacima, ukoso zarežite do srednje trećine.
1. Premažite sredinu senfom, pa djelomično ih preklapajući, posložite šnite šunke čitavom sredinom. Po šunki rasporedite pripremljeni nadjev, a zatim opet sve pokrijte šunkom.
1. Sad počnite plesti pletenicu. Zatvorite krajeve, a zatim naizmjence preklapajte sredinu trakama tijesta koje ste prethodno zarezali s obje strane.
1. Tako dobivenu pletenicu premažite žumanjkom razmućenim s malo mlijeka pa pecite 30 minuta na 200°C.

## Link

[Receptarka](https://receptarka.com/slana-jela/510-pletenica-sa-sunkom)
