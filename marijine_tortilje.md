# Marijine tortilje

### Sastojci

- 2 pakovanja tortilja
- 2 luka
- 500g piletine
- 2 paprike
- 2 mrkve
- 1 tikvica
- 1 češanj češnjaka
- crveni grah u limenci
- kukuruz u limenci
- paradajz sos
- 1 svježi krastavac
- 1 kiselo vrhnje 250g

### Priprema glavno jelo

1. Marinirati piletinu s uljem, soli, paprikom, limunom, šećerom i vegetom 
1. Ispržiti luk i dodati nasjeckanu piletinu
1. Dodati nasjeckanu papriku i tikvicu, naribanu mrkvu i ostaviti malo da se poprži
1. Dodati grah, kukuruz i paradajz sos
1. Uma

### Priprema umak

1. Izribati svježi krastavac
1. Iscjediti izribani krastavac u vrhnje i dodati naribani češnjak

### Link

[Marachupa](https://www.facebook.com/vasa.savjest)
