# Missina piletina

### Sastojci

- 1 pileća prsa
- pola kile tjestenine (ovisi koliko ljudi ima)
- 1 juha od gljiva
- 400ml vrhnja za kuhanje

### Priprema

1. Pileća prsa izrezati na kockice i ispržiti ih
1. Juhu ubaciti u 250ml vode i razmutiti
1. Kada pileća prsa dobiju boju, ubaciti vrhnje za kuhanje i razmućenu juhu
1. Skuhati tjesteninu i poslužiti

### Link

[Misso chef](https://www.facebook.com/misotheking)
