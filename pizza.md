# Pizza

## Umak

[Link](https://www.coolinarika.com/recept/savrseni-pizza-umak/?meta_refresh=1)

### Sastojci

- Pola luka
- 1 češanj
- 3-4 žlice koncentrata od rajčice 
- 1.5 dcl vode
- žlica soli
- origano
- vegeta
- biber
- 2 žlice ketchupa

### Priprema

1. Sitno nasjeckajte luk i češnjak pa propirjajte na malo ulja. (ja sam koristila suncokretovo)
1. Dok se luk i češnjak pirjaju stavite sve sastojke u jednu posudu i dobro izmješajte te dodajte na omekšali luk i češnjak. Sve pirjajte još 15-ak minuta i umak je spreman!

## Tijesto

[Link](https://www.coolinarika.com/recept/pizza-tijesto/?meta_refresh=1)

### Sastojci

### Priprema
