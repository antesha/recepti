# Emilijina piletina s rižom

### Sastojci

- 500g piletine
- 250g riže
- 2 batata
- 2 luka
- jedan češanj češnjaka
- 1-2 paprike

### Priprema

1. Izmarinirati piletinu u vegeti, kurkumi, limunu, biberu i peršinu
1. Ispržiti luk i češnjak na ulju
1. Dodati batat, papriku i piletinu i ostaviti ih da se malo poprže
1. Preliti sve vodom (do vrha)
1. Dodati rižu unutra
1. Kada sve ispari, dodati vrhnje za kuhanje
1. Sačekati da se zgusne i tjt

### Link

[Emilia](https://www.facebook.com/emilia.batrac)
