# Pita s mesom i krumpirom za 4 osobe

### Sastojci

- 350g Glatkog brašna
- otprilike 150ml tople vode
- 400g mljevenog mesa
- 400g krumpira
- 1 manja glavica crvenog luka
- sol, papar
- 1 žličica Vegete


### Priprema

1. U brašno dodajte soli i po malo dodavajući toplu vodu umijesite meko glatko tijesto. Tijesto se ne smije ljepiti za ruke, znači mijesiti dok ruke ne ostanu skroz čiste.

Potrebno ga je dobro izmijesiti, barem 5 min da postigne elastičnost, pa dobro premazati uljem i staviti da odmara.

2. Krumpir očistiti i isjeckati na sitne kockice. 

3. U krumpir umiješati mljeveno meso, sitno sjeckani luk i začine. Ja uvijek dodam i malo vode da se sve lakše razmješa...

4. Na plahtu staviti tijesto i lagano ga razvlačiti na sve strane, nadjev razbacati po cijelom tijestu ravnomjerno i lagano podizati plahtu sa obje strane kako bi se pita srolala.
5. Ugrijati pećnicu na 230C i staviti peči pitu oko 30min, nakon 20min pečenja poprskati pitu sa malo vruće vode.