# Svinjski lungić iz pećnice

### Sastojci

- 1 kg krumpira
- 1 lungić (500g)
- 250g šampinjona
- 200ml vrhnja za kuhanje
- 2 žlice brašna
- 1 paprika
- 1 luk
- 1 batat
- 2 češnja češnjaka

### Priprema

1. Tepsiju obložiti papirom za pečenje i 2/3 nauljiti i pobacati začine na tepsiju, krumpire prerezati po pola i 
1. Krumpire prerezati po pola i okrenuti ih prema tepsiji
1. Na 1/3 slobodnog prostora staviti 3-4 sloja aluminijske folije i oblikovati ju kao posudu
1. Gljive, papriku, luk, batat i češnjak pouljiti i začiniti u posebnoj zdjeli
1. Svinjski file začiniti, pouljiti i staviti u "posudu", nakon toga dodati i začinjeno povrće
1. "Posudu" pokriti jednim slojem aluminijske folije
1. Tepsiju staviti u pećnicu na 200°C na 30 minuta
1. U zdjeli pomješati vrhnje za kuhanje s brašnom i začiniti
1. Izvaditi tepsiju, ukloniti aluminijsku foliju s lungića i ubaciti vrhnje za kuhanje s brašnom
1. Vratiti tepsiju u pećnicu na 20 minuta

### Link

[Coolinarika](https://www.coolinarika.com/recept/svinjki-file-lungic-iz-pecnice/)
